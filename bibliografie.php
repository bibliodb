<?php
require_once("view/CiteView.php");
require_once("profiller.php");
Profiller::speed("Za��n�me");
require_once("CiteConfig.php");
require_once("CiteOutput.php");
//
require_once("CiteFields.php");
require_once("CiteData.php");
require_once("database.php");
require_once("model/Models.php");
require_once("model/DataCache.php");
require_once("model/CiteModel.php");
require_once("view/Template.php");
require_once("view/HtmlTemplate.php");
require_once("view/LatexTemplate.php");
Profiller::speed("Na�ten� vkl�dan�ch soubor�");

class isoRecord{
      var $header;
      var $directory=array();
      var $records=array();
      var $convertTable=array(
          "450"=>"keywords_cs",
          "460"=>"keywords_en",
          "010"=>"author",
          "100"=>"title",
          "030"=>"owner",
          "200"=>"source",
          "220"=>"year",
          "440"=>"annotation",
          "240"=>"pages",
          "210"=>"volume",
          "230"=>"number",
          "250"=>"vybaveni",
          "470"=>"kody_ministerstva",
          "780"=>"kody_obsahu",
          "790"=>"kody_vyu�it�"
      );
      public $data=array();
      function isoRecord($recordString){
          $this->records=explode('#',$recordString);
          $metadata=$this->cleanNumbers($this->records[0]);
          $this->header=substr($metadata,0,24);
          $this->directory=str_split(substr($metadata,25),12);
          $i=1;
          foreach($this->directory as $s){
             $tag=strlen($s)>3 ? substr($s,0,3) : $s;
             $tag=$this->convertField($tag);
             if(array_key_exists($tag,$this->data))$this->data[$tag][]=$this->cleanNumbers($this->records[$i]);
             else $this->data[$tag]=array($this->cleanNumbers($this->records[$i]));
             $i++;
          }     
      }
      function convertField($s){
          $s=strlen($s)>3 ? substr($s,0,3) : $s;
          if(array_key_exists($s,$this->convertTable)) return $this->convertTable[$s];
          else 
          return $s;
      }
      function getField($tag){
         return $this->data[$tag];
      }
      function pretyPrint(){
         echo "<tr><td>Tisknu</td><td>{$this->header}</td></tr>\n";
         //echo "<tr><td>".count($this->directory)."</td><td>".count($this->records)."</td></tr>\n";
         for($i=0;$i<count($this->directory);$i++){
            $field=$this->convertField($this->directory[$i]);
            echo "<tr><td>".$field."</td><td>".$this->cleanNumbers($this->records[$i+1])."</td></tr>\n";
         
         }
      }
      function cleanNumbers($s){
         return iconv("CP852","UTF-8",str_replace(array("\n","\r",'
'),'',$s));
      }
}

class Bibliography{
   private $iso_file_name="";
   private $record_delimiter="##";
   private $records=array();
   public function __construct($file_name){
      $this->iso_file_name=$file_name;
   }
   public function recordDelimiter($delimiter){
     $this->record_delimiter=$delimiter;
     return $this;
   }
   function loadRecords($count,$start=0){
     $iso=file_get_contents($this->iso_file_name);
     $records=explode($this->record_delimiter,$iso);
     $record="";
     for($i=$start;$i<$count;$i++){
          $r=new isoRecord($records[$i]);
          $this->records[]=$r->data;
     }
     
     return $this;
   }
   
   public function printBibliography(){
     foreach($this->records as $record){
       echo implode("; ",$record["title"])."<br />";
       //print_r($record->data);
     }
     echo count($this->records);
   }
}

$bib=new Bibliography("../EKO07.ISO");
$bib->recordDelimiter("##")
  ->loadRecords(20)
  ->printBibliography()
  ;
?>