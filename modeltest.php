<?php 
ob_start();
require_once("view/CiteView.php");
require_once("profiller.php");
Profiller::speed("Začínáme");
require_once("CiteConfig.php");
require_once("CiteOutput.php");
//
require_once("CiteFields.php");
require_once("CiteData.php");
require_once("database.php");
require_once("model/Models.php");
require_once("model/DataCache.php");
require_once("model/CiteModel.php");
require_once("view/Template.php");
require_once("view/HtmlTemplate.php");
require_once("view/LatexTemplate.php");
Profiller::speed("Načtení vkládaných souborů");
//CiteConfig::setValue("output_filter","latex");
class Publication{
   private $data=array();
   public function add($key,$data){ 
     if(array_key_exists($key,$this->data)){
       if(is_array($this->data[$key])){
            //CiteOutput::factory("Je pole")->lineBreak()->out();
            $this->data[$key][]=$data;
       }else{ 
         //CiteOutput::factory("Neni pole")->lineBreak()->out();
         $old=$this->data[$key];
         $this->data[$key]=array($old,$data);
       }
     }else{
       $data=is_array($data)?array($data):$data;
       $this->data[$key]=$data;
     }
   }
   public function fetch(){
      return $this->data;
   }
}
class pokus extends CiteModel{
    private $authors=null;
    private $publikace=array();
    public function bibliography(){
       CSL::style("iso-690")
        //->citation()
        //->end()
        ->macro("author")
          ->field("names")->variable("author")->suffix(".")->limit(3)->limitUse(3)
              ->name()->delimiter("; ")->delimiterAnd("; ")
                ->namePart("family")->textCase("uppercase")->end()
              ->end()
          ->end()
        ->end()  
        ->macro("editor")
             ->field("names")->variable("editor")->suffix(".")->limit(3)->limitUse(3)
              ->name()->delimiter("; ")->delimiterAnd("; ")
                ->namePart("family")->textCase("uppercase")->end()
              ->end()
          ->end()
        ->end()
        ->macro("primary")
             ->field("choose")
                ->cslIf()->variable("editor")
                  ->field("text")->macro("editor")->end()
                ->cslElse()
                  ->field("text")->macro("author")->end()
                ->end()  
             ->end()
        ->end()  
        ->macro()
        ->end()
        ->macro("")
        ->end()
        ->bibliography()
          ->sort()->end()
          ->layout()
            ->field("text")->macro("primary")->end()
            ->field("text")->macro("document_name")->end()
          ->end()
         ->end();  
            //->suffix(".")
            /*->field("names")->variable("author")->suffix(".")->limit(3)->limitUse(3)
              ->name()->delimiter("; ")->delimiterAnd("; ")
                ->namePart("family")->textCase("uppercase")->end()
              ->end()
            ->end()
            ->field("base")->variable("title")
              ->fontStyle("italic")->fontWeight("bold")->textCase("title")
            ->end()
           ->field("base")->macro("publisher")->end() 
           ->field("base")->variable("date")->end()
           ->field("group")
             ->field("base")->term("pokus")->prefix(":")->suffix(";")->end()
             ->field("base")->variable("titlle")->end()
             ->field("number")->variable("ohoo")->end()
            ->end()
            ->field("choose")
              ->cslIf()->variable("pokusíík title")->match("any")->field(text)->value("První podmínka")->end()
              ->cslElseIf()->variable("title")
                ->field("text")->value("druhá podmínka")->end()
              //->end()
              ->cslElse()->field("text")->value("nic z toho")->end()
              ->end()
            ->end()
          ->end()
        ->end(); */
        /*->field("names")
          ->variable("author")
          ->limit(3)
          ->limitUse(3)
          ->name()
            ->delimiter("; ")
            ->delimiterAnd("; ")
            ->namePart("family")
               ->textCase("uppercase")
            ->end()
          ->end()
        ->end()
        ->field("base")
         ->variable("title")
         ->fontStyle("italic")
         ->fontWeight("bold")
         ->textCase("title")
       ->end()
       ->field("base")
         ->variable("publisher")
        ->end() 
        ->field("base")
          ->variable("date")
        ->end();*/
        
        CSL::printBibliography();
    }
    public function __construct(){
      $this->db=DatabaseConnector::database("krokobib")
      //->user("root")
      //->password("root")
      ->connect();
      Profiller::speed("Připojení k databázi");
      try{
        $name=ModelFactory::get("Name");
        $publication=ModelFactory::get("Publication");
        $authors=ModelFactory::get("Author");
        $role=ModelFactory::get("AuthorRole");
        $publication_type=ModelFactory::get("PublicationType");
        //$res=$this->query("autori")->rawQuery("SELECT name.* FROM publication inner join author on publication_id=publication inner join name on author_name=name_id ")->result();
        Profiller::speed("Nahrání modelů");
        $res=$this->query("autori")
          ->select("name.name_id, family,given, part, von_part, date, description, publication, author_id, author_name,role")
          ->link($authors,$publication)
          ->link($authors,$role)
          ->link($authors,$name)
          ->where("author_name is not null order by family")
          ->buildQuery()
          //Profiller::speed("Build query");
          ->prepare()->execute()->result();
          //->buildQuery()->rawQuery()->result();
          Profiller::speed("Query autoři");
        $this->authors=new DataCache($authors);
        $this->names=new DataCache($name);
        while($row=$res->fetch(PDO::FETCH_ASSOC)){
           $this->authors->load($row,"publication");
           $this->names->load($row);
        }
        //$this->authors->load($res);
        Profiller::speed("Nahrání autorů");
        //$this->show($this->authors->fetch(1));
        //$this->show($this->authors->fetch(2));
        //$this->show($this->authors->fetch(1));
        //Profiller::speed("Dotazy na autory");
        //$this->show($this->authors->fetch(5));
        $publikace=$this->query("publikace")
           ->select("publication_id, type")
           ->link($publication,$publication_type)
           ->buildQuery()->prepare()->execute()->result();
        Profiller::speed("Query publikace");
        while($row=$publikace->fetch(PDO::FETCH_ASSOC)){
           $id=$row["publication_id"];
           $type=$row["type"];
           CiteOutput::factory("Publikace: $id typ: $type")->lineBreak()->out();
           $authors=$this->authors->get($id);
           $publ=new Publication();
           if(is_array($authors)){
             foreach($authors as $author){
                $role=$author["role"];
                $publ->add($role,$author);
                $publ->add("type",$type); 
                //CiteOutput::factory("autor")->lineBreak()->out();
                //CiteOutput::factory(implode(" / ",$author))->lineBreak()->out();             
                //$this->show($author);
             }
           }else{
              //CiteOutput::factory("Autoři nejsou pole")->lineBreak()->out();
           }
           $publ->add("publisher","Nakladatel");
           $publ->add("date","2009");
           $publ->add("title","titulek");
           $publ->add("author",array("family"=>"Obecný","given"=>"Autor"));
           //print_r($publ->fetch());
           $this->publikace[]=$publ->fetch();
            
        }
        Profiller::speed("Nahrání publikací");     
      }catch(Exception $e){
         CiteOutput::factory($e->getMessage())->lineBreak()->out();
      }
      CiteData::loadData($this->publikace);
      Profiller::speed("Nahrání do CiteData");
      //ModelFactory::get("name")->retrieve(2); 
    }
}
$pokus=new Pokus();
$pokus->bibliography();
Profiller::speed("Konec");
Profiller::show();
$content=ob_get_contents();
$content.=CiteView::get();
ob_end_clean();
//echo $content;
$tpl=new HtmlTemplate();
$tpl->assert("page_contents",$content);
$tpl->assert("page_title",CiteOutput::factory(CiteConfig::getValue("page_title"))->get());
echo $tpl->get("index.php");
?>