<?php  
class CSL{
    //Subnodes
    private $info=null;
    private $macros=array();
    private $locale=null;
    private $citation=null;
    private $bibliography=null;
    private $name="";
    private $lang="en";
    private $types=array();
    final private function __clone() {}
    final private function __construct() {
      $this->addFilter("base","CiteprocField");
      $this->addFilter("names","CiteprocNames");
      $this->addFilter("text","CSLText");
      $this->addFilter("number","CSLNumber");
      $this->addFilter("group","CSLGroup");
      $this->addFilter("choose","CSLChoose");
      $this->citation=new CSLCitation($this);
      $this->bibliography=new CSLBibliography($this);
      
    }

    final private static function getInstance()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new self();
        }
        return $instance;
    }
    
    public function style($name){
        $instance=self::getInstance();
        $instance->name=$name;
        return $instance; 
    }
    public function citation(){
        $instance=self::getInstance();
        return $instance->citation;
    }
    public function bibliography(){
        $instance=self::getInstance();
        return $instance->bibliography;
    }
    
    public function macro($macro_name){
        $instance=self::getInstance();
        $macro=new CSLMacro($instance);
        $instance->macros[$macro_name]=$macro;
        return $macro;
    }
    public function useMacro($macro_name=""){
        $instance=self::getInstance();
        if(array_key_exists($macro_name,$instance->macros))
           return $instance->macros[$macro_name]->get();
        return false;
    }
    public function lang($lang){
        $instance=self::getInstance();
        $instance->lang=$lang;
        return $instance;
    }
    public function useTerm($term, $lang){
        $instance=self::getInstance();
        $lang=$lang?$lang:$instance->lang;
        return "$term-$lang";
    }
    public function cite(){
      //ToDo: Generate citekey
       $instance=self::getInstance();
      return $instance->citation->get();
    }
    public function printBibliography($records=false){
      $instance=self::getInstance();
      CiteData::records($records);
      $out=CiteOutput::factory()->noEscape()->template("bibstart");
      while(CiteData::current()===true){
        $cite=$instance->cite();
        $bib=$instance->getBibRecord();
        $out->text($instance->formatBibRecord($cite,$bib));
        CiteData::next();
        Profiller::speed("Položka bibliografie");
      }
      $out->template("bibend");
      $out->out();
      return $this;
    }
    public function formatBibRecord($citation, $record){
       return CiteOutput::factory()->noEscape()->text("$record")->template("@bibliography/entry")->get();
    }
    public function getBibRecord(){
      $instance=self::getInstance();
      return $instance->bibliography->get();
      //$out=CiteOutput::factory()->noEscape();
      //foreach($instance->fields as $field){
       //   $out->text($field->get());
       // }
       //return $out->get(); 
    }
    public function field($type){
      $instance=self::getInstance();
      if(array_key_exists($type,$instance->types)){
        $c=new $instance->types[$type]($instance);
        $instance->fields[]=$c;
        return $c;
      }else{
        throw new Exception("Output filter \"$type\" doesn´t exist");
      }
      
    }
    public function getField($type,$parent){
       $instance=self::getInstance();
       if(array_key_exists($type,$instance->types)){
        $c=new $instance->types[$type]($parent);
        //$instance->fields[]=$c;
        return $c;
       }else{
        throw new Exception("Output filter \"$type\" doesn´t exist");
       }
    }
    function addFilter($name,$class){
      $this->types[$name]=$class;
    }
}

class CSLBase{
    public $data=null;
    protected $value=null;
    protected $fields=array();
    protected $parent_class=null;   
    public $variable=null;
    protected $delimiter=",";
    protected $actions=array();
    protected $form=null;
    protected $term=null;
    protected $lang=null;
    public $macro=null;
    function __construct($parent){
      $this->parent_class=$parent;
    }
    public function setData($data){
      $this->data=$data;
      return $this;
    }
    public function setter($variable,$value){
      $this->$variable=$value;
      return $this;
    }
    public function variable($name){
      $this->variable=$name;
      return $this;
    }
    public function macro($macro_name){
      $this->macro=$macro_name;
      return $this;
    } 
    public function term($term_name,$lang=false){
      //$lang = language of cited document
      $this->term=$term_name;
      $this->lang=$lang;
      return $this;
    }
    public function value($value){
      $this->value=$value;
      return $this;
    }
    public function form($form="long"){
      //form = long / short / verb / verb-short / symbol
      $this->form =$form;
      return $this;
    }
    protected function getData($field=null){
       if($field!=null){
         return CiteData::getList($field);
       }else if($this->macro!=null){
         //echo "Použiju makro<br />";
         return CSL::useMacro($this->macro);
       }else if($this->data){  
         return $this->useFormat($this->data);
       }else if($this->value){
         return $this->value;
       }else if($this->term){
         return CSL::useTerm($this->term, $this->lang);
       }else{
         echo "Nic z toho<br />";
         return "";
       }
     }
    public function end(){
      //print_r($this->parent_class);
      return $this->parent_class;
    }
    protected function useFormat($data){
       return implode($this->delimiter,$data);
    }
    public function field($type){
        $c=CSL::getField($type,$this);
        $this->fields[]=$c;
        return $c;
    }
}


class CSLTop extends CiteprocField{
     protected $sort=null;
     protected $layout=null;
     public function __construct($parent){
       parent::__construct($parent);
       $this->sort=new CSLSort($this);
       $this->layout=new CSLLayout($this);
     }  
     public function sort(){return $this->sort;}
     public function layout(){return $this->layout;}
     public function get(){return $this->layout->get();}
     public function getSortOptions(){$this->sort->get();}
}

class CSLCitation extends CSLTop{ 
     public function get(){
       $author=array_shift($this->getData("author"));
       $author=$author["family"].", ".$author["given"];
       $year=$this->getData("date");
       $key=CiteData::getCiteKey();
       return "[$author($year)]{{$key}}";
     } 
}

class CSLBibliography extends CSLTop{
     public function get(){
        return $this->layout->get();
     }
}
class CSLSort extends CiteprocField{
     public function get(){
        return $this->layout->get();
     }
}
class CSLSubFields extends CiteprocField{
  public function get(){
       $out=CiteOutput::factory()->noEscape();
      foreach($this->fields as $field){
          $out->text($field->get());
        }
       return $out->get();
  }
}
class CSLLayout extends CSLSubFields{
}
class CiteprocField extends CSLBase{
    public $prefix="";
    public $suffix="";
    //public $macros=array();
    //public $data;
    protected $font_style=array("normal"=>"@font-style/normal","italic"=>"@font-style/italic","oblique"=>"@font-style/oblique");
    protected $font_variant=array("normal"=>"@font-variant/normal","small-caps"=>"@font-variant/small-caps");
    protected $font_weight=array("normal"=>"@font-weight/normal","bold"=>"@font-weight/bold","light"=>"@font-weight/light");
    protected $text_decoration=array("none"=>"@text-decoration/none","underline"=>"@text-decoration/underline");
    protected $vertical_align=array("baseline"=>"@vertical-align/baseline","sup"=>"@vertical-align/sup","sub"=>"@vertical-align/sub");
    protected $styles=array();
    
    protected $text_case="";

    public function __construct($parent){
      parent::__construct($parent);
      $this->prefix($this->parent_class->prefix)
        ->suffix($this->parent_class->suffix);                              
    }
    
    public function prefix($s){
      $this->prefix=$s;
      return $this;
    }
    public function suffix($s){
      $this->suffix=$s;
      return $this;
    }
    //style functions
    public function fontStyle($style){return $this->style($this->font_style,$style);}
    public function fontVariant($style){return $this->style($this->font_variant,$style);}
    public function fontWeight($style){return $this->style($this->font_weight,$style);}
    public function textDecoration($style){return $this->style($this->text_decoration,$style);}
    public function verticalAlign($style){return $this->style($this->vertical_align,$style);}
    public function style($stylearray,$style){
       if(array_key_exists($style, $stylearray)){
         $this->styles[]=$stylearray[$style]; 
       }
       return $this;
    }
    public function textCase($case){$this->text_case=$case;return $this;}
    
    protected function useStyle($s){
       $format=CiteOutput::factory($s)->textFunction($this->text_case);
       foreach($this->styles as $style)$format->template($style);
       return $format->get();
    }
    //protected function getData(){
    //   return CiteData::getField($this->variable);
    //}
    protected function usePrefix(){return $this->prefix;}
    protected function useSuffix($s=null){
      $last_char=substr(trim($s),-1);
      return ($last_char==$this->suffix)?"":$this->suffix;
    }
    public function get(){   
      $data= $this->getData($this->variable); 
      if($data){         
        return $this->usePrefix().$this->useStyle($data).$this->useSuffix($data);
      }else{
        return false;
      }
    }
    
    public function printError(Exception $e){
      echo "<div style=\"border:1px solid black;color:red;\">".error_message.$e->getMessage()."</div>\n";
    }
}

class CiteprocList extends CiteprocField{
  protected $limit=false;
  protected $limit_use=false;
  
  public function limit($limit){
    $this->limit=$limit;
    return $this;
  }
  public function limitUse($use){
    $this->limit_use=$use;
    return $this;
  }
  protected function processField($field,$last=false){
     return implode(", ",$field); 
  }
  public function get(){
     try{                            
       $data=CiteData::getList($this->variable);
       $data_len=count($data);
       if($data_len==0)return false;
       $max=$this->limit_use?$this->limit_use:$this->limit;
       $counter=$this->limit?$this->limit:$data_len;
       $counter=($counter >= $data_len)?$data_len:$max;
       $return_value=$this->usePrefix(); 
       //CiteOutput::factory("Použijeme $counter položek. Maximum je $max, $položek v databázi bylo $data_len")->out();
       $fields=array();
       for($i=0;$i<$counter;$i++){
          //$return_value.=$this->processField($data[$i]);
          $last=($i==($counter-2))?1:0;
          $last=($i==($counter-1))?-1:$last;
          $return_value.=$this->processField($data[$i],$last);
       }
       //if(count($fields)>1){
       //   $delim=array_slice($fields,0,count($fields)-1);
       //   $last=array_slice($fields,-1);
       //   $return_value.=implode($this->useDelimiter(),$delim).$this->use
       //}
       $return_value.=$this->useSuffix($return_value);
       return $return_value;
       //CiteOutput::factory("Použijeme $counter položek. Maximum je $max, $položek v databázi bylo $data_len")->out();
       
     }
     catch(Exception $e){
       CiteOutput::factory("There is some problem with data: ".$e->getMessage())->out();;
     }
  } 
   
}


class CiteprocDataSet extends CiteprocField{
   public $data=null;
   public function setData($data){
    $this->data=$data;
    return $this;
  }
  protected function getData($variable=null){
    return $this->useFormat($this->data); 
  }
}

//CitationStyle Names element 
class CiteprocNames extends CiteprocList{
  // 
  public $suffix=".";
  protected $names=null;
  public function __construct($parent){
    parent::__construct($parent);
    $this->names=new CiteprocName($this);
  }
  protected function processField($data,$last=false){
    $this->names->setData($data);
    $add="";
    if($last==0){
      $add=$this->names->useDelimiter();
    }else if($last>0){
      $add=$this->names->useDelimiterPrecedesLast();
    }
    //CiteOutput::factory("Oddělovač $add")->lineBreak()->out();
    return "{$this->names->get()}$add";
  }
  public function name(){
    return $this->names;
  }
}

class CiteprocName extends CiteprocDataSet{
  //Musí být zabalen ve třídě CiterpocNames
 
  private $parts=array();
  protected $delimiter=",";
  protected $_and=" & ";
  protected $precedes_last="never";
  
  public function useDelimiter(){ return $this->delimiter;}
  public function useAnd(){return $this->_and;}
  public function useDelimiterPrecedesLast(){
     switch($this->precedes_last){
     case "always":
       return $this->delimiter.$this->_and;
     break;
     default:
       return $this->_and;
     }     
  }
  
  public function namePart($part_name){
     $this->parts[$part_name]=new CiteprocNamePart($this);
     return $this->parts[$part_name];
  }
  public function delimiter($s){
     $this->delimiter=$s;
     return $this;
  }
  public function delimiterAnd($s){
     $this->_and=$s;
     return $this;
  }
  protected function usePart($part,$data){
     if(array_key_exists($part,$data)){
        if(array_key_exists($part,$this->parts)){
          return $this->parts[$part]->setData($data)
            ->variable($part)
            ->get();
          //return $this->parts[$part]->get();
        }else{
          return $data[$part];
        }
     }else{
       return $part; 
     }
  }
  protected function useFormat($part){
     $parts=array("family",", ","given");
     $return_value ="";
     foreach($parts as $element){
        $return_value.=$this->usePart($element,$part);
     }
     return $return_value;
     //return implode(", ",array($part[family],$part["given"]));
  }
  //public function get(){
   //  return $this->usePrefix().$this->useStyle($this->getData()).$this->useSuffix();
  //}
}
class CiteprocNamePart extends CiteprocDataSet{
     public function getData($variable){
       return $this->data[$variable];
     }
} 


class CSLMacro extends CSLSubFields{}
class CSLText extends CiteprocField{
}
class CSLNumber extends CiteprocField{}
class CSLGroup extends CSLSubFields{
   public function get(){
      $out=CiteOutput::factory()->noEscape();
      $condition=false;
      $checked_values=0;
      //$out->text();
      foreach($this->fields as $field){
         $result=$field->get();
         if($field->variable OR $field->macro){
            $sub_condition=strlen($result)>0?true:false; 
            $condition=($condition || $sub_condition);
            $checked_values++;
         }
         $out->text($result);
      }
      //$out->text($this->useSuffix());
      if($condition===false AND $checked_values>0){
        return false;
      } else{
        return $this->usePrefix().$this->useStyle($out->get()).$this->useSuffix();
      }
  }
}

class CSLChoose extends CiteprocField{
   protected $ifs=array();
   protected $elses=null;
   public function cslIf(){
       $if= new CSLIf($this);
       $this->ifs[]= $if;
       return $if;
   }
   public function cslElseIf(){
       return $this->cslIf();
   }
   public function cslElse(){
       $else=new CSLIf($this);
       $this->elses=$else;
       return $else;
   }
   public function get(){
       $pruchod=0;
       $trues=array();
       foreach($this->ifs as $condition){
          $pruchod++;
          $result=$condition->get();
          //CiteOutput::factory("Tak a result je $result, průchod $pruchod")->lineBreak()->out();
          if(strlen($result)>0){
            $trues[]=array("disambiguate"=>$condition->getDisambiguate(),"content"=>$result);
          }
       }
       //CiteOutput::factory("Tak a result je $result, průchod $pruchod")->lineBreak()->out();
       if(count($trues)>0){
          foreach($trues as $disambig){
             if($disambig["disambiguate"])return $disambig["content"];
          }
          return $trues[0]["content"];
       }
       if($this->elses)return $this->elses->get();
       return false;
   }
}

class CSLIf extends CSLSubFields{
    protected $disambiguate=false;
    protected $conditions=array();
    protected $matches="all";
    protected function setCondition($condition_name,$values){
        $chunks=explode(" ",$values);
        $this->conditions[$condition_name]=$chunks;
        return $this;
    }
    //public function cslIf(){return $this->parent_class->cslIf();}
    public function cslElseIf(){ return $this->parent_class->cslElseIf();}
    public function cslElse(){return $this->parent_class->cslElse();}
    public function getDisambiguate(){
       return $this->disambiguate;
    }
    //conditional functions
    public function disambiguate($disambiguate=true){
       $this->disambiguate=$disambiguate;
       return $this;
    }
    public function isNumeric($variable){return $this->setCondition("IsNumeric",$variable);}
    public function isUncertainDate($variable){return $this->setCondition("IsUncertain_date",$variable);}
    public function locator($variable){return $this->setCondition("Locator",$variable);}
    public function position($variable){
      //Implementovat v budoucnu, zatím nemá smysl
      return $this;
    }
    
    public function type($type){return $this->setCondition("Type",$type);}
    
    public function variable($variable){return $this->setCondition("Variable",$variable);}
    
    public function match($match="all"){return $this->setter("matches",$match);} 
    
    protected function testIsNumeric($variable){return is_numeric(CiteData::getData($variable));}
    protected function testIsUncertainDate($variable){return false;}
    protected function testLoacator($variable){return CiteData::getData("locator")==$variable;}
    protected function testPosition($position){return false;}
    protected function testType($type){return CiteData::getData("type")==$type;}
    protected function testVariable($variable){return CiteData::getData($variable)!==null;}
    protected function logical($conditions){
       if(count($conditions)==1)return $conditions[0];
       $sum=$this->matches=="all"?true:false;
       foreach($conditions as $cond){
          $suma=$cond?"true":"false";
          if($this->matches=="any"){
            $sum=($sum OR $cond);
          }else{
             $sum=($sum AND $cond);
          }
       }
       return $this->matches=="none"?!$sum:$sum;
    }
    
    protected function testCondition($condition_name,$values){
       $conditions=array();
       $condition_method="test$condition_name";
       foreach($values as $element){
         $conditions[]=$this->$condition_method($element);
       }
       return $this->logical($conditions);
    }
    
    public function get(){
        $condition=array();
        foreach($this->conditions as $sub_condition=>$values){
           $condition[]=$this->testCondition($sub_condition,$values);
        }
        //CiteOutput::factory(implode(" - ",$condition))->lineBreak()->out();
        if($this->logical($condition))return parent::get();
        if(!count($this->conditions)>0)return parent::get();
        return false;
    }
    
}
    
?>