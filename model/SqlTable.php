<?php

class SqlTable{
     private $_parent=null;
     private $table=array();
     private $engine=null;
     private $encoding=null;
     private $collation=null;
     private $primary=null;
     private $foreign=array();
     private $table_name="";
     private $meta=array();
     public function __construct($parent=null){
        $this->_parent=$parent;
     }
    public function create(){
       //Funkce na vytvoření tabulky
       //ToDo: Vytvořit funkční kód pro CREATE TABLE
       $encoding=$this->encoding?"CHARACTER SET {$this->encoding}":null;
       $collation=$this->collation?"COLLATE = {$this->collation}":null;
    }
    public function retrieve(){
       return "SELECT ".implode(", ",$this->table)." FROM {$this->table_name} WHERE {$this->primary}=?";
    }
    public function update(){}
    public function delete(){}
    
    public function table($table_name){
       $this->table_name=$table_name;
       return $this;
    } 
    public function column($column_name, $type,$null,$comment){
       $this->table[]=$column_name;
       $this->meta[$column_name]=array("type"=>$type,"null"=>$null,"comment"=>$comment);
       return $this;
    }
    public function primary($column_name){
       $this->primary=$column_name;
       return $this;
    }
    public function foreign($column_name,$table=""){
       $this->foreign[$table]=$column_name;
       return $this;
    }
    public function getPrimary(){
       return $this->primary;
    }
    public function getForeign($table){
       if(array_key_exists($table,$this->foreign))return $this->foreign[$table];
       else throw new Exception("There is no foreign key for table $table");
    }
    public function getTableName(){
       return $this->table_name;
    }
}
?>