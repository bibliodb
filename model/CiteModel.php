<?php 
class CiteModel{
  public $db=null;
  private $queries=array();
  public function __construct(){ 
  }
  public function show($row){
    CiteOutput::factory(implode(" / ",array_values($row)))->lineBreak()->out();
  }
  
  public function query($query_name){
    CiteOutput::factory($query)->lineBreak()->out();
    if(!array_key_exists($query,$this->queries))
       $this->queries[$query_name]=new CiteQuery();  
    return $this->queries[$query_name];
  }
}

class CiteQuery{
  private $current_query="";
  private $prepared_query=null; 
  private $select_part=array(); 
  private $from_part=array();
  private $where_part=array();
  private $first_link=true;
  private $inner_part=array();
  private $exec=false;
  private function setter($variable,$value){
     array_push($this->$variable,$value);
     return $this;
  }
  public function select($select){return $this->setter("select_part",$select);}
  
  public function from($from){
     $this->from_part[]=$from;
     return $this;
  }
  
  public function where($where){return $this->setter("where_part",$where);}
  
  protected function inner($inner){
    $this->inner_part[]=$inner;
    return $this;
  }
   
  public function link($primary,$foreign){
     $primary_key=$primary->getPrimary();
     $primary_table=$primary->getTableName();
     $foreign_table=$foreign->getTableName();
     $query=$this->first_link?"$primary_table ":"";
     try{
       $foreign_key=$foreign->getForeign($primary_table);
     }catch(Exception $e){
       try{
         $foreign_key=$primary->getForeign($foreign_table);
         $primary_key=$foreign->getPrimary();
         $query.="inner join $foreign_table on $primary_table.$foreign_key=$foreign_table.$primary_key";
         $this->first_link=false;
         return $this->inner($query);
       }
       catch(Exception $e){
         CiteOutput::factory("There is no foreign key in table $foreign_table for table $primary_table")->italics($e->getMessage())->lineBreak()->out();
       } 
       //return $this;
     }                                                                                     
     $query=$this->first_link?"$primary_table ":"";
     $query.="inner join $foreign_table on $primary_table.$primary_key=$foreign_table.$foreign_key";
     $this->first_link=false;
     //CiteOutput::factory("Inner Join:")->italics($query)->lineBreak()->out();
     return $this->inner($query);  
  }
  
  public function buildQuery(){
     $select=implode(", ",$this->select_part);
     $from=implode(", ",$this->from_part);
     $from=implode(" ",$this->inner_part);
     $where=count($this->where_part)>0?" WHERE ".implode(" AND ",$this->where_part):"";
     $this->current_query="SELECT $select FROM $from$where;";
     //$this->prepare();
     CiteOutput::factory("Databázový dotaz:")->italics($this->current_query)->lineBreak()->out();
     Profiller::speed("Build query");
     return $this;
  } 
  public function prepare($query=null){
     $query=$query!==null?$query:$this->current_query;
     //CiteOutput::factory("Databázový dotaz:")->italics($query)->lineBreak()->out();
     $this->statement=DatabaseConnector::prepare($query);
     //Profiller::speed("Prepare");                        
     return $this;
  }
  public function execute(){
     if(is_object($this->statement))
        $this->exec= $this->statement->execute();
     else 
        throw new Exception("There is no prepared statement");
     //Profiller::speed("Exec");   
     return $this;   
  }
  public function rawQuery($query=null){
     $query=$query?$query:$this->current_query;
     //CiteOutput::factory("Databázový dotaz:")->italics($query)->lineBreak()->out();
     $this->statement=DatabaseConnector::rawQuery($query);
     return $this;
  }
  public function result(){
     if(is_object($this->statement))
        return $this->statement;
     else
       print_r(DatabaseConnector::errorInfo());
       throw new Exception("There is no database result");
  }
}
?>