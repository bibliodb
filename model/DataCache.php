<?php
//ToDo: 
class DataCache{
  private $data=array();
  private $model=null;
  public function __construct($model){
     $this->model=$model;
    
     //$this->statement=$parent->prepare($model->retrieve());
     $this->statement=DatabaseConnector::prepare($model->retrieve());
  }
  public function load($data,$primary=null){
     $primary=$primary?$primary:$this->model->getPrimary();
     $citac=0;
     if(is_object($data)){
       CiteOutput::factory("Nahrávám objekt")->lineBreak()->out();
       while($row=$data->fetch(PDO::FETCH_ASSOC)){
         $this->data[$row[$primary]][]=$row;
         $citac++;
       }
     }else if(is_array($data)){
        //CiteOutput::factory("Nahrávám pole")->italics($primary)->lineBreak()->out();
        $this->data[$data[$primary]][]=$data;
        $citac++; 
     } else{
        //CiteOutput::factory("Nahrávám nevím co")->lineBreak()->out();
        Throw new Exception("You tried to load unknown type of data to DataCache");
     }                    
     CiteOutput::factory("Nahrál jsem $citac řádků")->lineBreak()->out;
  }
  public function get($id){
      return array_key_exists($id,$this->data)?$this->data[$id]:null;
  }
  public function fetch($id){
     if(array_key_exists($id,$this->data)){
       CiteOutput::factory("Beru data z cache")->lineBreak()->out();
       return $this->data[$id];
     }else{
       //ToDo: Měl by mít vlastní query, ta by měla být kompilovaná, v současnosti se každá qery provádí zvlášť
       //$res=$this->_parent->query($this->model->retrieve($id));
       $res=$this->statement->execute(array($id));
       if($res>0){
          $this->load($this->statement);
          if(array_key_exists($id,$this->data)){
             return $this->data[$id];
          }else{
             Throw new Exception("Cannot find $id in results from database");
          } 
       }else{
          Throw new Exception("Cannot find $id in database");
       }
       
     }                      
  } 
}
?>