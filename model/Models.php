<?php
require_once("./model/SqlTable.php");
/*class ModelName extends SqlTable{
  public function __construct($parent){
    parent::__construct($parent);
    $this->table("name")
      ->column("name_id")
      ->column("family")
      ->column("given")
      ->column("von_part")
      ->column("date")
      ->column("part")
      ->column("description")
      ->primary("name_id")
  }
}
*/


class ModelFactory{
   public static function get($model_name,$parent=null){
      $class_name="Model".$model_name;
      if(class_exists($class_name))return new $class_name($parent);
      else throw new Exception("Model $model_name doesn´t exists");
   }
}
class ModelAuthor extends SqlTable{
  public function __construct($parent){
    parent::__construct($parent);
    $this->table("author")
      ->column("author_id", "int(11)", "Ne", "")
      ->column("author_name", "int(11)", "Ne", "Odkazuje do tabulky names")
      ->column("author_role", "int(11)", "Ne", "Odkazuje do tabulky autor_role")
      ->column("publication", "int(11)", "Ne", "Odkaz na publikaci")
      ->primary("author_id")
      ->foreign("author_name","name")
      ->foreign("publication","publication")
      ->foreign("author_role","author_role");
  }
}
class ModelAuthorRole extends SqlTable{
  public function __construct($parent){
    parent::__construct($parent);
    $this->collate="InnoDB";
    $this->table("author_role")
      ->column("id", "int(11)", "Ne", "")
      ->column("role", "varchar(30)", "Ne", "jméno role (např. author, editor, apod.)")
      ->primary("id");
  }
}  

class ModelLang extends SqlTable{
  public function __construct($parent){
    parent::__construct($parent);
    $this->table("lang")
    ->column("lang", "varchar(8)", "Ne", "zkratka jazyka")
    ->column("lang_name", "varchar(30)", "Ne", "")
    ->primary("lang");
  }
}

class ModelName extends SqlTable{
  public function __construct($parent){
    parent::__construct($parent);
    $this->table("name")
    ->column("name_id","int(11)","Ne","")
    ->column("given", "varchar(100)", "Ne", "Křestní jméno")
    ->column("family", "varchar(255)", "Ne", "Příjmení")
    ->column("von_part", "varchar(10)", "Ne", "von část jména - např. Lufwig van Beethoven")
    ->column("date", "varchar(20)", "Ne", "Datum narození, smrti")
    ->column("part", "text", "Ne", "např. u jmen konferencí, nebo asijských jmen")
    ->column("description", "text", "Ne", "Rozlišovací údaje o autorovi")
    ->primary("name_id");
  }
}
class ModelPublication extends SqlTable{
  public function __construct($parent){
    parent::__construct($parent);
    $this->table("publication")
    ->column("publication_id", "int(11)", "Ne", "")
    ->column("publication_type", "int(11)", "Ne", "Typ publikace - odkazuje se k publication_type")
    ->primary("publication_id")
    ->foreign("publication_type","publication_type");
  }
}

class ModelPublicationType extends SqlTable{
  public function __construct($parent){
    parent::__construct($parent);
    $this->table("publication_type")
    ->column("publication_type_id", "int(11)", "Ne", "")
    ->column("type", "varchar(30)", "Ne", "Typ publikace - kniha, sborník, atd.")
    ->primary("publication_type_id");
  }
}

    
class ModelTitleRole extends SqlTable{
  public function __construct($parent){
    parent::__construct($parent);
    $this->table("title_role")
    ->column("title_role_id", "int(11)", "Ne", "")
    ->column("role_name", "varchar(20)", "Ne", "typ názvu dokumentu - originál, překlad, apod")
    ->primary("title_role_id");
  }
}
?>