<pre>
<?php 
require_once("profiller.php");
Profiller::speed("Začínáme");
require_once("CiteConfig.php");
require_once("CiteOutput.php");
require_once("CiteData.php");
require_once("CiteFields.php");
require_once('Text/LanguageDetect.php');
require_once("database.php");
require_once("model/Models.php");
Profiller::speed("Načtení vkládaných souborů");
//echo CiteConfig::getValue("pokus");
CiteConfig::setValue("output_filter","html");
//echo CiteConfig::getValue("output");
//CiteOutput::factory()->template("@font-style/italic","Italickej")->template("@font-weight/bold","tlustej")->lineBreak()->out();
//CiteOutput::factory("Nazdar ")->lineBreak()->bold("světe")->textFunction("lowercase","Velká Písmenka")->text("normálka")->out();
//CiteOutput::factory()->lineBreak()->startBlock("citation")->text("Nazdar")->bold("světe")->closeBlock("citation")->out();

CiteConfig::setValue("output_filter","latex");
//CiteOutput::factory()->lineBreak()->startBlock("citation")->text("Nazdar")->bold("světe")->closeBlock("citation")->out();

/*CiteFields::field("base")->variable("title")->fontStyle("italic")->fontWeight("bold")->textCase("title");
CiteFields::field("base")->variable("publisher");
CiteFields::field("base")->variable("date");
*/
CSL::style("iso-690")
    ->citation()
    ->end()
    ->field("base")
     ->variable("title")
     ->fontStyle("italic")
     ->fontWeight("bold")
     ->textCase("title")
   ->end()
   ->field("base")
     ->variable("publisher")
    ->end() 
    ->field("base")
      ->variable("date")
    ->end();
    Profiller::speed("Styl");
$data= array(
  array(
    "author"=>array(
      array("given"=>"Jan","family"=>"Krka"),
      array("given"=>"Martin","family"=>"Blažek"),
      array("given"=>"Pepa","family"=>"Nosek"),
      array("given"=>"Jirka","family"=>"Cicek"),
      array("given"=>"Martin","family"=>"Blažek")),
    "title"=>"Titulek knihy",
    "date"=>"2009",
    "publisher"=>"Academia"
    ),
    array(
    "author"=>array(
      array("given"=>"Jan","family"=>"Krka"),
      array("given"=>"Martin","family"=>"Blažek"),
      array("given"=>"Pepa","family"=>"Nosek"),
      array("given"=>"Jirka","family"=>"Cicek"),
      array("given"=>"Martin","family"=>"Blažek")),
    "title"=>"Tohle je úplně jiná knihA",
    "date"=>"2009",
    "publisher"=>"od úplně jinýho s nebez<pečím> nakladatelství"
    )
  );
CiteData::loadData($data);
Profiller::speed("Nahrání datového pole do CiteData");
CSL::printBibliography();
Profiller::speed("Tisk bibliografie");

$l = new Text_LanguageDetect();
Profiller::speed("Nahrání třídy LanguageDetect");
$res=$l->detect("Takže to češtinou nemá zas tak hrozný problémy Zdá se, že ne. A co řikáš, ty?");
Profiller::speed("Zjištění jazyka");
$l->detect("A další jazyk");
Profiller::speed("Zjištění dalšího jazyka");
//print_r($res);
DatabaseConnector::database("krokobib")
  ->user("root")
  ->password("root")
  ->connect();
Profiller::speed("Připojení k databázi");
DatabaseConnector::query("publikace")
   ->table("publication")
      ->column("publication_type")
      ->foreign("publication_type","publication_types")
      ->primary("publication_id")
   ->table("publication_types")
      ->column("type")
      ->primary("publication_type_id")   
   //->link("publication","author")
   ->link("publication_types","publication")
   ->where("publication_id=1")
 ->end()
 ->query("author")
    ->table("publication")
      ->column("publication_type")
      ->foreign("publication_type","publication_types")
      ->primary("publication_id")
    ->table("name")
      ->column("family")
      ->column("given")
      ->primary("name_id")
     ->table("author_role")
       ->column("role")
       ->primary("id") 
     ->table("author")
      ->primary("author_id")
      ->foreign("author_name","name")
      ->foreign("publication","publication")
      ->foreign("author_role","author_role")
      ->link("name","author")
     ->link("publication","author")
     ->link("author_role","author")
     ->where("publication_id=5")
   ->end()    
->run();
Profiller::speed("Databázový dotaz");
echo CiteView::get();
Profiller::show();
//CiteOutput::factory()->noSpaces()->template("@font-style/italic","Italickej")->template("@font-weight/bold","tlustej")->template("@bibliography/entry")->template("bibstart")->template("bibend")->out();


/*SELECT t3.publication_type, t1.family, t1.given, t4.role
FROM publication AS t3
INNER JOIN author AS t2 ON t3.publication_id = t2.publication
INNER JOIN name AS t1 ON t1.name_id = t2.author_name
INNER JOIN author_role AS t4 ON t4.id = t2.author_role*/
?>