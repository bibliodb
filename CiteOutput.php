<?php
require_once("view/CiteView.php");
class CiteOutput{
       //enforce singleton
    private $types=array();   
    final private function __clone() {}
    final private function __construct() {
      $this->addFilter("html","OutputHtml");
      $this->addFilter("latex","OutputLatex");
    }

    final private static function getInstance()
    {
        static $instance = null;

        if (null === $instance) {
            $instance = new CiteOutput();
        }
        return $instance;
    }
    
    public function factory($arg=""){
      $instance=self::getInstance();
      $type=CiteConfig::getValue("output_filter");
      if(array_key_exists($type,$instance->types)){
        return new $instance->types[$type]($arg);
      }else{
        throw new Exception("Output filter \"$type\" doesn´t exist");
      }
    }
    function addFilter($name,$class){
      $this->types[$name]=$class;
    }
}

class OutputFilter{
   protected $blocks=array();
   protected $s;
   protected $escape=true; 
   protected $nospace=false;
   protected $spaces=false;
   protected $filters=array();
   protected $functions=array();
  function __construct($string){
    $this->s=$this->escape($string);
    $this->addFunction("lowercase","TextLowercase");
       $this->addFunction("uppercase","TextUppercase");
       $this->addFunction("capitalize-first","TextCapitalizeFirst");
       $this->addFunction("title","TextTitleCase");
  }
  
  protected function addFunction($name,$class){
       $this->functions[$name]=new $class;
  }
  
  protected function add($s){
    $space="";
    if(strlen($this->s)>0){
      if(substr($this->s,-1)!==" " && substr($this->s,0)!==" " && !$this->nospace && !$this->spaces)$space=" ";
    }
    $this->s.=$space.$s;
    $this->nospace=false;
    return $this;
  }
  
   public function noSpaces(){$this->spaces=true;return $this;}
   public function spaces(){$this->spaces=false;return $this;}
   
   protected function escape($s){
     if(!$this->escape)return $s;
     return htmlspecialchars($s); 
   }
   public function noEscape(){$this->escape=false;return $this;}
   protected function stringGet($s){
     //
     if($s){$s= $this->escape($s);}
     else{
        $s=$this->s;
        $this->s="";
     }
     return $s;
   }
   protected function tag($tag,$s,$params=array()){
     return $this->startTag($tag,$params).$this->escape($s).$this->closeTag($tag,$params);
   }
   
   protected function startTag($tag,$params){
     $p="";
     foreach($params as $key=>$value){
       $p.=" $key=\"".$this->escape($value)."\"";
     }
     return "<$tag$p>";
   } 
   protected function closeTag($tag,$params){
     return "</$tag>";
   }
   public function out(){
     CiteView::out($this->s);
     return $this;
   }
   public function get(){
     return $this->s;
   }
   public function textFunction($function, $s=false){
       $s=$this->stringGet($s); //if parameter $s is empty, then $s will be $this->s
       if(array_key_exists($function,$this->functions)){
         $s= $this->functions[$function]->execute($s);
       }
       $this->add($s);
       return $this;
    }
   
   public function template($format,$string=false){
     $string=$this->stringGet($string);
     if(array_key_exists($format,$this->formats))$this->add(sprintf($this->formats[$format],$string));
     else throw new Exception("Output template $format doesn´t exist");
     return $this;
   }
   
   public function startBlock($block){
     $tag=array_key_exists($block,$this->blocks)?$this->blocks[$block]:null;
     $this->add($this->startTag($tag,array()));
     return $this;
   }
   public function closeBlock($block){
     $tag=array_key_exists($block,$this->blocks)?$this->blocks[$block]:null;
     $this->add($this->closeTag($tag,array()));
     return $this;
   }
}

//ToDo: More TextCase handlers. Unimplemented:
//"capitalize-first": capitalizes the first character; the case of other characters is not affected
//"capitalize-all": capitalizes the first character of every word; other characters are displayed lowercase
//"sentence": displays text in sentence case ("sentence style")

class TextLowercase{public function execute($s){return mb_convert_case($s,MB_CASE_LOWER,"UTF-8");}}
class TextUppercase{public function execute($s){return mb_convert_case($s,MB_CASE_UPPER,"UTF-8");}}
class TextCapitalizeFirst {public function execute($s){return mb_convert_case($s,MB_CASE_TITLE,"UTF-8");}}
class TextTitleCase{public function execute($s){return mb_convert_case($s,MB_CASE_TITLE,"UTF-8");}}



interface BaseOutput{
   function bold($s);
   function italics($s);
   function lineBreak();
   function text($s);
   function template($format,$s=false);
}
class OutputHtml extends OutputFilter implements BaseOutput{  
   protected $formats=array(
     "bibstart"=> "<div class=\"csl-bib-body\">\n%s",
	   "bibend"=> "%s</div>",
  	"@font-style/italic"=> "<i>%s</i>",
  	"@font-style/oblique"=> "<em>%s</em>",
  	"@font-style/normal"=> "<span style=\"font-style=>normal;\">%s</span>",
  	"@font-variant/small-caps"=> "<span style=\"font-variant=>small-caps;\">%s</span>",
  	"@font-variant/normal"=> "%s",
  	"@font-weight/bold"=> "<b>%s</b>",
  	"@font-weight/normal"=> "<span style=\"font-weight=>normal;\">%s</span>",
  	"@font-weight/light"=> "%s",
  	"@text-decoration/none"=> "<span style=\"text-decoration=>none;\">%s</span>",
  	"@text-decoration/underline"=> "<span style=\"text-decoration=>underline;\">%s</span>",
  	"@vertical-align/baseline"=> "%s",
  	"@vertical-align/sup"=> "<sup>%s</sup>",
  	"@vertical-align/sub"=> "<sub>%s</sub>",
  	"@bibliography/entry"=> "<div class=\"csl-entry\">%s</div>\n"
   );
   protected $blocks=array("citation"=>"quote");
   public function bold($s){
     $this->add($this->tag("strong",$s));
     return $this;
   }
   public function italics($s){
     $this->add($this->tag("i",$s));
     return $this;
   }
   public function lineBreak(){
     $this->add("<br />\n");
     return $this;
   }
   
   public function text($s){
     $this->add($this->escape($s));
     return $this;
   }
      
}

class OutputLatex extends OutputFilter implements BaseOutput{
  protected $blocks=array("citation"=>"cite");
  protected $formats=array("bibstart"=> "\\begin{thebibliography}\n%s",
	"bibend"=> "%s\\end{thebibliography}\n",
	"@font-style/italic"=> "\\textit{%s}",
	"@font-style/oblique"=> "\\emph{%s}",
	"@font-style/normal"=> "\\textnormal{%s}</span>",
	"@font-variant/small-caps"=> "\\textsc{%s}",
	"@font-variant/normal"=> "\\textnormal{%s}",
	"@font-weight/bold"=> "\\textbf{%s}",
	"@font-weight/normal"=> "\\textnormal{%s}",
	"@font-weight/light"=> "%s",
	"@text-decoration/none"=> "\\texnormal{%s}",
	"@text-decoration/underline"=> "\\underline{%s}",
	"@vertical-align/baseline"=> "%s",
	"@vertical-align/sup"=> "\\textsuperscript{%s}",
	"@vertical-align/sub"=> "\\ensuremath{_{\\textrm{\\scriptsize %s}}}",
	"@bibliography/entry"=> "\n\\bibitem %s\n",

);
  protected function startTag($tag,$params){
    return "\\$tag{";
  }
  protected function closeTag($tag,$params){
    return "}";
  } 
  public function bold($s){
    $this->add($this->tag("textbf",$s));
    return $this; 
  }
  public function italics($s){
     $this->add($this->tag("textit",$s));
     return $this;
   }
   public function lineBreak(){
     $this->add("\\\\\n");
     return $this;
   }
   public function text($s){
     $this->add($this->escape($s));
     return $this;
   }
   public function startBlock($block){
     $tag=array_key_exists($block,$this->blocks)?$this->blocks[$block]:null;
     $this->add("\n\\begin{".$tag."}\n");
     $this->nospace=true;
     return $this;
   }
   public function closeBlock($block){
     $this->nospace=true;
     $tag=array_key_exists($block,$this->blocks)?$this->blocks[$block]:null;
     $this->add("\n\\end{".$tag."}\n");
     return $this;
   }
}


?>