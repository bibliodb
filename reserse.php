<pre>
<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once("view/CiteView.php");
require_once("profiller.php");
Profiller::speed("Začínáme");
require_once("CiteConfig.php");
require_once("CiteOutput.php");
//
require_once("CiteFields.php");
require_once("CiteData.php");
require_once("database.php");
require_once("model/Models.php");
require_once("model/DataCache.php");
require_once("model/CiteModel.php");
require_once("view/Template.php");
require_once("view/HtmlTemplate.php");
require_once("view/LatexTemplate.php");
Profiller::speed("Načtení vkládaných souborů");

class Publication{
   private $data=array();
   public function add($key,$data){ 
     if(array_key_exists($key,$this->data)){
       if(is_array($this->data[$key])){
            //CiteOutput::factory("Je pole")->lineBreak()->out();
            $this->data[$key][]=$data;
       }else{ 
         //CiteOutput::factory("Neni pole")->lineBreak()->out();
         $old=$this->data[$key];
         $this->data[$key]=array($old,$data);
       }
     }else{
       $data=is_array($data)?array($data):$data;
       $this->data[$key]=$data;
     }
   }
   public function fetch(){
      return $this->data;
   }
}

class Reserse{
  private $sources=array();
  private $counter=0;
  public function head(){
    $number_of_records=$this->counter;
    setlocale(LC_COLLATE, 'czech');
    usort($this->sources, 'strcoll'); 
    CiteOutput::factory()->lineBreak()
    ->bold("Rešerše:")->text("č.j. 4 / 2010")->lineBreak()
    ->bold("Téma rešerše:")->text("Systém CDS/ISIS pro využití v knihovní praxi")->lineBreak()
    ->bold("Zadavatel:")->text("Národní technická knihovna pro účely RKK")->lineBreak()
    ->bold("Zpracovatel:")->text("Michal Hoftich")->lineBreak()
    ->bold("Datum zadání:")->text("1. 4. 2010")->lineBreak()   
    ->bold("Datum zpracování:")->text("29. 4. 2010")->lineBreak()
    ->bold("Klíčová slova:")->text("CDS/ISIS")->lineBreak()
    ->bold("Jazykové vymezení:")->text("Angličtina, Čeština, Slovenština")->lineBreak()
    ->bold("Časové rozmezí:")->text("1998 -- 2010")->lineBreak()
    ->bold("Uspořádání záznamů:")->text("abecedně dle zdroje a následně chronologicky (dle data vydání)")->lineBreak()
    ->bold("Zdroje:")->text(implode(", ",$this->sources))->lineBreak()
    ->bold("Celkový počet záznamů: ")->text($number_of_records)->lineBreak()
    ->bold("Typ záznamů:")->text("citace dle normy ČSN ISO 690 a ČSN ISO 690/2 s anotací")->lineBreak()
    ->out();
    return $this;
  }
  public function addSource($name){
     $this->printSource(0);
     $this->sources[]=$name;
     $this->actual=$name;
     return $this;
  }
  public function load($file_name){
     $contents=file_get_contents($file_name);
     $records=explode("\zalomit",$contents);
     $this->printSource(count($records));
     foreach($records as $record) {
       if(strlen(trim($record))>0)
       CiteOutput::factory()->text("\item")->text($record)->out();
     }
     //$this->sources[$this->actual]=$records; 
      return $this;
  }
  public function loadEbsco(){
     $this->ebsco=new Ebsco();
     include("ebsco.php");
     $count = $this->ebsco->load($ebsco);
     $this->printSource($count);
     $this->ebsco->bibliography();
     return $this;
  }
  
  public function printSource($counter){
    if($this->actual){
      $this->counter+=$counter; 
      CiteOutput::factory()->lineBreak()->bold("Název zdroje")->text($this->actual)->lineBreak()
        ->bold("Počet záznamů")->text($counter)->lineBreak()->out();
      $this->actual=false;
    }
  }
}
function ebscocmp($a,$b){return strcmp($a["year"],$b["year"]);}
class Ebsco{
  private $publ=array(); 
  function load($ebsco){
    $i=0;
    $data=array();
    
    foreach($ebsco as $zaznam){
      $publ=new Publication();
      $mala=array();
      $i++;
      if(array_key_exists("Author",$zaznam)){
        $autori=explode("; ",$zaznam["Author"]);
        $n=array();
        foreach($autori as $autor){
          $parts=explode(", ",$autor);
          $p=array("family"=>$parts[0],"given"=>$parts[1]);
          $n[]=$p;
        }
        $zaznam["Author"]=$n;
      }
      if(array_key_exists("ISSN",$zaznam)){
        $issn=implode("-",str_split($zaznam["ISSN"],4));
        $zaznam["ISSN"]=$issn;
        
      }
      foreach($zaznam as $key=>$value){
        $klic=strtolower($key);
        $mala[$klic]=$value;
      }
      $data[]=$mala;
      //$data[]=$publ->fetch();
      echo "<pre>";
      //print_r($data);
      echo "</pre>";
      //CiteOutput::factory($i.": ".$zaznam["Author"])->lineBreak()->out();
    }
    usort($data,"ebscocmp");
    //echo "<pre>";
      //print_r($data);
      //echo "</pre>";
    CiteData::loadData($data);
    return count($data);
  } 
  public function bibliography(){
     CSL::style("iso-690")
     ->bibliography()
          ->sort()->end()
          ->layout()
            ->field("group")
              ->field("names")->variable("author")->suffix(".")->limit(3)->limitUse(3)
                  ->name()->delimiter("; ")->delimiterAnd("; ")
                    ->namePart("family")->textCase("uppercase")->end()
                  ->end()
              ->end()
            ->end()
            ->field("text")->variable("title")->suffix("")->end()
            ->field("text")->variable("journal")->fontStyle("italic")->suffix(".")->end()
            ->field("text")->variable("year")->suffix(",")->end()
            ->field("group")
              ->field("text")->value("vol.")->end()
              ->field("text")->variable("volume")->suffix(",")->end()
            ->end()
            ->field("group")
              ->field("text")->value("no.")->end()
              ->field("text")->variable("number")->suffix(",")->end()
            ->end()
            ->field("group")
              ->field("text")->value("p.")->end()
              ->field("text")->variable("pages")->suffix(".")->end()
            ->end()
            ->field("text")->value("Dostupné také komerčně na WWW: <\url{http://search.ebscohost.com/}>.")->end()
            ->field("group")
              ->field("text")->value("ISSN")->end()
              ->field("text")->variable("issn")->suffix(".")->end()
             ->end() 
             ->field("group")
               ->field("text")->value("//dělit anotaci//")->end()
               ->field("text")->variable("abstract")->end()
             ->end()
        ->end()
      ->end();  
     CSL::printBibliography();
  }
}
CiteConfig::setValue("output_filter","html");
$reserse=new Reserse();
$reserse
->addSource("ANL -- Článková bibliografie")
->load("./reserse-kurz/anl.txt")
->addSource("Česká národní bibliografie")
->addSource("EBSCO")
->loadEbsco()
->addSource("KKL -- Katalog Knihovnické Literatury")
->load("./reserse-kurz/kkl.txt")
->addSource("Souborný katalog ČR -- Monografie")
->addSource("Souborný katalog ČR -- Seriály")
->addSource("Centrální katalog Univerzity Karlovy")
->addSource("Volný internet")
->head(3);
//$ebscoClass=new Ebsco();
//include("ebsco.php");
//$ebscoClass->load($ebsco)
//->bibliography();
$content=CiteView::get();
//$tpl=new LatexTemplate();
//$content.=print_r(dba_handlers(true),true);
$tpl=new HtmlTemplate();
$tpl->assert("page_contents",$content);
$tpl->assert("page_title","rešerše");
echo $tpl->get("index.php");
?>