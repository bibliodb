<?php 
require_once('../simpletest/unit_tester.php');
require_once('../simpletest/reporter.php');
require_once("CiteData.php");
class TestData extends UnitTestCase{
  
  function testLoadData(){
    $data= array(
  array("type"=>"author","value"=>array("first"=>"Jan","last"=>"Krka")),
  array("type"=>"author","value"=>array("first"=>"Martin","last"=>"Bla�ek")),
  array("type"=>"author","value"=>array("first"=>"Pepa","last"=>"Nosek")),
  array("type"=>"author","value"=>array("first"=>"Jirka","last"=>"Cicek")),
  array("type"=>"author","value"=>array("first"=>"Martin","last"=>"Bla�ek")),
  array("type"=>"title","value"=>"Titulek knihy"),
  array("type"=>"date","value"=>"2009"),
  array("type"=>"publisher","value"=>"Academia")
  );
  $p=Data::getInstance()->loadData($data);
  $this->assertEqual($p->getField("title"),"Titulek knihy");
  }
  
  function testSingletonData(){
    $this->assertEqual(Data::getInstance()->getField("title"),"Titulek knihy");
  } 
}

class TestOutput extends UnitTestCase{

}

$test = new TestData();
$test->run(new HtmlReporter())
?>