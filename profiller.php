<?php
class Profiller{
       //enforce singleton
    protected $time_log=array(); 
    protected $last_time=0;  
    final private function __clone() {}
    final private function __construct() {
    }

    final private static function getInstance()
    {
        static $instance = null;

        if (null === $instance) {
            $instance = new self();
        }
        return $instance;
    }
    public function show(){
       $inst=self::getInstance();
       echo "<table>\n<tr><th>Message</th><th>Time</th></tr>\n";
       foreach($inst->time_log as $log){
          echo "<tr><td>{$log[message]}</td><td>{$log[time]}</td></tr>\n";
       }
       echo "</table>\n";
    }
    public function speed($message){
      $curr_time=microtime();
      $inst=self::getInstance();
      $last_time=($inst->last_time>0)?$inst->last_time:$curr_time;
      $ellapsed=$curr_time-$last_time;
      $inst->time_log[]=array("time"=>$ellapsed,"message"=>$message);
      $inst->last_time=microtime();
    }
    
}
?>