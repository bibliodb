<?php 
class DatabaseConnector{
   private $database_name="";
   private $database_type="mysql";
   private $host="localhost";
   private $user="";
   private $password="";
   private $db=null;
   private $lastQueryName="default";
   private $results=array();
   private $queries=array();
   public function __construct(){
      $this->host=CiteConfig::getValue("database_host");
      $this->user=CiteConfig::getValue("database_user");
      $this->password=CiteConfig::getValue("database_password");
      $this->database_name=CiteConfig::getValue("database_name");
   }
   final private static function getInstance()
    {           
        static $instance = null;
        if (null === $instance) {
            $instance = new self();
        }
        return $instance;
    }
    private function setter($variable,$value){
       $inst=self::getInstance();
       $inst->$variable=$value;
       return $inst;
    }
    public function database($db_name){return self::setter("database_name",$db_name);}
    public function user($user){return self::setter("user",$user);}
    public function password($password){return self::setter("password",$password);}
    public function connect(){
        $inst=self::getInstance();
        $query="{$inst->database_type}:host={$inst->host};dbname={$inst->database_name}";
        try{
          $inst->db=new PDO($query,$inst->user,$inst->password,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        }
        catch(PDOException $e){CiteOutput::factory("Nastala chyba při připojování k databázi: ".$e->getMessage())->linebreak()->italics($query)->out();}
        return $inst;
    }
    public function query($query_name="default"){
        $inst=self::getInstance();
        $inst->queries[$query_name]=new Query($inst);
        $inst->lastQueryName=$query_name;
        return $inst->queries[$query_name];
    }
    
    public function rawQuery($query){
        $inst=self::getInstance();
        return $inst->db->query($query);
    }
    public function prepare($query){
        $inst=self::getInstance();
        return $inst->db->prepare($query);
    }
    public function errorInfo(){
        $inst=self::getInstance();
        return $inst->db->errorInfo();
    }
    public function run($query=null){
        $inst=self::getInstance();
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); 
        $query=$query?$query:$inst->lastQueryName;
        $this->results[$query]= $this->db->query($this->queries[$query]->buildQuery());
        echo $this->queries[$query]->buildQuery()."<br />\n";
        //$res=$inst->db->query($this->queries[$query]->buildQuery());
        foreach($this->results[$query] as $row){
          print_r($row);
        }
    }
}


class Query{
  private $parentClass=null;
  private $query="";
  private $tables=array();
  private $lastTable;
  private $alias=array();
  private $primary=array();
  private $foreign=array();
  private $links=array();
  private $aliasCount=1;
  private $where=array();
  public function __construct(DatabaseConnector $parent=null){
    $this->parentClass=$parent;
  }
  public function end(){
    return $this->parentClass; 
  }
  public function table($table_name){
     $this->tables[$table_name]=array();
     $this->lastTable=$table_name;
     return $this;
  } 
  public function alias($alias){
     $this->alias[$this->lastTable]=$alias;
     return $this;
  }

  public function column($column_name){
     $this->tables[$this->lastTable][]=$column_name;
     return $this;
  }
  public function primary($column_name){
     $this->primary[$this->lastTable]=$column_name;
     return $this;
  }
  public function foreign($column_name,$table=""){
     $this->foreign[$this->lastTable][$table]=$column_name;
     return $this;
  }
  public function link($primary,$foreign){
     if(array_key_exists($primary,$this->primary) AND array_key_exists($foreign,$this->foreign) AND array_key_exists($primary,$this->foreign[$foreign])){ 
       $this->links[]=$this->getAlias($primary).".".$this->primary[$primary]."=".$this->getAlias($foreign).".".$this->foreign[$foreign][$primary];
     }
     return $this;
  }
  public function where($where){
     $this->where[]=$where;
     return $this;
  }
  private function getAlias($table){
     if(array_key_exists($table,$this->alias))return $this->alias[$table];
     else $this->alias[$table]="t{$this->aliasCount}";
     $this->aliasCount++;
     return $this->getAlias($table);
  }
  private function buildSelect($select_part){
     return is_array($select_part)?"SELECT ".implode(", ",$select_part):""; 
  }
  private function buildFrom($table_part){
     return is_array($table_part)?" FROM ".implode(", ",$table_part):""; 
  }
  private function buildWhere($where_part){
     return is_array($where_part)?" WHERE ".implode(" AND ",$where_part):""; 
  }
  
  public function buildQuery(){
     $select_part=array();
     $table_part=array(); 
     foreach($this->tables as $table=>$columns){
        $alias=$this->getAlias($table);
        foreach($columns as $col){
          $select_part[]="$alias.$col";
        }
        $table_part[]="$table AS $alias";
     }
     $this->query=$this->buildSelect($select_part).$this->buildFrom($table_part).$this->buildWhere($this->links);
     return $this->query;
  }
}
class Result{}
?>