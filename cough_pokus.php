<?php
include_once('coughphp/load.inc.php');
include_once('coughphp/as_database/load.inc.php');
CoughDatabaseFactory::addConfig(array(
    // db_name_hash is a hash of alias name => actual db name (in the default
    // case, db_name_used_during_generation => environment_db_name). For example,
    // if you generate on `*_dev` databases and are setting up your test config,
    // then the hash might look like this:
    'db_name_hash' => array(
        'app_db_dev' => 'krokobib',
        'reports_dev' => 'krokobib'
    ),
    'driver' => 'mysql',
    'host' => 'localhost',
    'user' => 'root',
    'pass' => 'root',
    'port' => 3306
));
?>
