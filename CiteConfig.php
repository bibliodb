<?php
class CiteConfig{
    private $conf=array("pokus"=>"Nazdar");
       //enforce singleton
    final private function __clone() {}
    final private function __construct() {}

    final public function getInstance()
    {
        static $instance = null;

        if (null === $instance) {
            $instance = new CiteConfig();
        }
        return $instance;
    }
    public  function getValue($key){
      $instance=self::getInstance();
      return $instance->conf[$key];
    }
    public function setValue($key, $value){
      $instance=self::getInstance();
      $instance->conf[$key]=$value;
    }
}
CiteConfig::setValue("output_filter","html");
CiteConfig::setValue("database_user","root");
CiteConfig::setValue("database_password","root");
CiteConfig::setValue("database_host","localhost");
CiteConfig::setValue("database_name","krokobib");
CiteConfig::setValue("encoding","utf-8");
CiteConfig::setValue("html_template_dir","view/tpl/html/");
CiteConfig::setValue("html_cache_dir","view/tpl/html/cache/");
CiteConfig::setValue("latex_template_dir","view/tpl/latex/");
CiteConfig::setValue("latex_cache_dir","view/tpl/latex/cache/");

CiteConfig::setValue("page_title","Pokus");
?>