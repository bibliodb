<?php
class LatexTemplate extends Template{
   public function __construct(){
      parent::__construct();
      try{
         $this->tplDir=CiteConfig::getValue("latex_template_dir");
         $this->cacheDir=CiteConfig::getValue("latex_cache_dir");
      }catch(Exception $e){}
   }
}
?>