<?php
class Template{
      var $tplDir='view/tpl/';
      var $cacheDir='view/tpl/cache/';
      var $data=array();
      var $syntax=array('{:',':}');
      var $semantics=array('<?php echo $','; ?');
      function __construct(){
           $this->semantics[1]=$this->semantics[1].'>';
      }
      function compile($tpl_file){
           $tpl=$this->tplDir.$tpl_file;
           return str_replace($this->syntax,$this->semantics,file_get_contents($tpl));    
      }
      function cachedFile($tpl_file){
         $cache_file=$this->cacheDir . $tpl_file;
         $orig_template=$this->tplDir.$tpl_file;
         if(file_exists($cache_file)){
           if(filemtime($orig_template)>filemtime($cache_file)){
              unlink($cache_file);
              return $this->cachedFile($tpl_file);
           }
           return $cache_file;                                        
         }else{
           try{
             file_put_contents($cache_file,$this->compile($tpl_file));
             return $this->cachedFile($tpl_file);
           } catch(Exception $e){
             //myerr($e,"Chyba pri ukladani sablony");
             echo "Chyba pri ukladani sablony: ".$exception-getMessage().'<br />';
           }     
         }
      }
      function assert($name,$data){
         $this->data[$name]=$data;
      }
      function get($tpl_file){
          extract($this->data);
          //extract($GLOBALS, EXTR_SKIP);
          ob_start();
          include($this->cachedFile($tpl_file));
          //require($tpl_file);
          $applied_template = ob_get_contents();
          ob_end_clean();
          return $applied_template;
      }
}
?>