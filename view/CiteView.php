<?php
class CiteView{
   private $content="";
   final private static function getInstance()
    {           
        static $instance = null;
        if (null === $instance) {
            $instance = new self();
        }
        return $instance;
    }
    public function out($s){
      $inst=self::getInstance();
      $inst->content.=$s;
    }
    public function get(){
       $inst=self::getInstance();
       return $inst->content;
    }
}
?>