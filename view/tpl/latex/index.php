<?php header('Content-type: text/latex');?>
\documentclass{article}
\usepackage[{:language:}]{babel}
\usepackage[{:inputenc:}]{inputenc}
\begin{document}
\title{{:page_title:}}
\maketitle

<?php 
echo $page_contents; 
?>
\end{document}