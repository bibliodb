<?php header('Content-type: text/latex');?>
\documentclass{article}
\usepackage[<?php CiteOutput::factory($language)->out(); ?>]{babel}
\usepackage[<?php CiteOutput::factory($inputenc)->out(); ?>]{inputenc}
\begin{document}
\title{<?php CiteOutput::factory($page_title)->out(); ?>}
\maketitle

<?php 
echo $page_contents; 
?>
\end{document}