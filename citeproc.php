<?php
$cas=microtime();
  /*Gener�tor citac�, podle specifikace citeproc
  */
  define("field_not_exist","field doesn`t exist");
  define("error_message","Error occured: ");
  define("bad_data","multiple fields, should be just one");
  
  class citeprocData{
    //
    private $data=array();
    private $fieldslist=array();
    
    public function loadData($data){
      for($i=0;$i<count($data);$i++){
        $this->data[$i]=$data[$i];
        $this->fieldslist[$data[$i]["type"]][]=$i;
      }
    }
    private function getData($field){
      if(array_key_exists($field,$this->fieldslist)){
        $list=array();
        foreach($this->fieldslist[$field] as $record){
          $list[]=$this->data[$record];
        }
        return $list;
      }else{
        throw new Exception(field_not_exist);
      }
    }
    public function getField($field){
      try{
        $list=$this->getData($field);
        if(count($list)==1){
          return($list[0]["value"]);
        }else{
          throw new Exception(bad_data);
        }
      }catch(Exception $e){
        $this->printError($e);
      }
    }
    public function getList($field){
        return $this->getData($field);
    }
  }
  
  class citeprocBase{
    public $prefix="";
    public $suffix="";
    public $macros=array();
    public $data;
    function __construct(citeprocData $data=null){
      $this->data=$data;
    }
    public function addMacro($name,&$code){
      $this->macros[$name]=$code;
    }
    
    public function get($value){
      return $this->prefix.$this->data->getField($value).$this->suffix;
    }
    
    public function printError(Exception $e){
      echo "<div style=\"border:1px solid black;color:red;\">".error_message.$e->getMessage()."</div>\n";
    }
  }
  
  class citeprocInfo extends citeprocBase{
        public $id="";
        public $title="";
        public $link="";
        public $author=array(array("name"=>"","email"=>"","uri"=>""));
        public $citation_format="";
        public $field="";
        public $updated="";
        public $summary="";
        public $rights="";
  }
  
  class citeprocBibliography extends citeprocBase{
    function printBibliography(array $fields){
      foreach($fields as $field=>$macro){
        $exec=$this->macros[$macro];
        echo $exec->get($field);
      }
    }
  }
  class citeprocCitation extends citeprocBase{}
  
  class citeprocField extends citeprocBase{
    public $locale;
    public $data;
    public $suffix=". ";
    public function __construct(citeprocData $data,$locale=null){
      $this->data=$data;
      $this->locale=$locale;
    }
  }
  class citeprocText extends citeprocField{}
  
  class citeprocDate extends citeprocField{
    public $form=array("year-month-day","year-month","year");
    /* "year-month-day" - default, displays year, month and day
    * "year-month" - displays year and month
    * "year" - displays year only*/
  }
  
  class citeprocList extends citeprocField{}
  
  class citeprocAuthor extends citeprocList{
    public $et_al_min=5;
    public $et_al_uses_first=3;
    public $and=" - ";
    public $delimiter=" - ";
    public $delimiter_precedes_last=array("");
    public $form="long";
    private function formatAuthor($author){
      return $author["last"].", ".$author["first"];
    }
    public function get($field){
      $authors=$this->data->getList($field);
      $v=array();
      if(count($authors)>0){
        $v[]= $this->prefix;
        count($authors)<$this->et_al_min ? $authorlist=count($authors) : $authorlist=$this->et_al_uses_first;
        $authorlist==count($authors) ? $etal="" : $etal=$this->locale->get("et-al");
        $m=array();
        for($i=0;$i<$authorlist;$i++){
          $m[]=$this->formatAuthor($authors[$i]["value"]);
        }
        $v[]=implode($this->delimiter,$m);
        $v[]=" $etal";
        $v[]= $this->suffix; 
      }else{
      $v[]="��dn� auto�i";
      }
      return $this->prefix.implode("",$v);
    }
  }
  
  
  class citeprocLocale extends citeprocBase{
    public $lang;
    public $locale=array("cs"=>array(
    "et-al"=>"et al"
    ));
    function get($c){
      return $this->locale[$this->lang][$c];
    }
  }
  
  class Logical{
    public $op=null;
    public $np=array();
    protected $n=array("kk","nn","pp");
    function nul(){
      $this->np=array();
      $this->op=null;
    }
    function getValue(){
      if(count($this->np)<1)return null;
      if($this->op==null){
        return array_shift($this->np);
      }else{
        $n=$this->op;
        $this->op=null;
        return $n;
      }
    }
    function is($c){
      $this->np[]=in_array($c,$this->n) ? true : false;
      return $this;               
    } 
    function a(){
      $p=true;
      $m=$this->getValue();
      while($m!==null){
        $p=$p&&$m;
        $m=$this->getValue();
      }
      $this->op=$p;
      return $this;
    }
    function o(){
      $p=false;
      $m=$this->getValue();
      while($m!==null){
        $p=$p||$m;
        $m=$this->getValue();
      }
      $this->op=$p;
      return $this;
      
    }
    function dump(){
      echo "<p>$this->op</p>";
      return $this;
    }
  }
  
  
  $citeData=new citeprocData();
  $data= array(
  array("type"=>"author","value"=>array("first"=>"Jan","last"=>"Krka")),
  array("type"=>"author","value"=>array("first"=>"Martin","last"=>"Bla�ek")),
  array("type"=>"author","value"=>array("first"=>"Pepa","last"=>"Nosek")),
  array("type"=>"author","value"=>array("first"=>"Jirka","last"=>"Cicek")),
  array("type"=>"author","value"=>array("first"=>"Martin","last"=>"Bla�ek")),
  array("type"=>"title","value"=>"Titulek knihy"),
  array("type"=>"date","value"=>"2009"),
  array("type"=>"publisher","value"=>"Academia")
  );
  $citeData->loadData($data);
  $locale=new citeprocLocale();
  $locale->lang="cs";
  //echo $citeData->getField("autnhor");
  $base=new citeprocField($citeData,$locale);
  $author=new citeprocAuthor($citeData,$locale);
  $bibliography=new citeprocBibliography($citeData);
  $bibliography->addMacro("base",$base);
  $bibliography->addMacro("author",$author);
  $bibliography->printBibliography(array(
  "author"=>"author",
  "title"=>"base",
  "date"=>"base",
  "publisher"=>"base"
  ));
  $c=new Logical;
  $c->is("nn")->is("kk")->is("kk")->a()->is("nn")->a()->is("nn")->a()->is("nnk")->o()->dump();
  echo (microtime()-$cas);
?>