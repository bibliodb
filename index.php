<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>z39.5 klient</title>
</head>
<body>
<?php

$host=$_REQUEST[host];
$query=stripslashes($_REQUEST[query]);
$num_hosts = count($host);
if (empty($query) || count($host) == 0) {
    echo '<form method="get">
    <input type="checkbox"
    name="host[]" value="bagel.indexdata.dk/gils" />
        GILS test
    <input type="checkbox"
    name="host[]" value="localhost:9999/Default" />
        local test
    <input type="checkbox" checked="checked"
    name="host[]" value="ckis.cuni.cz:9992/PEDF" />
        PedF UK
    <br />
    RPN Query:
    <input type="text" size="30" name="query" />
    <input type="submit" name="action" value="Search" />
    </form>
    ';        
} else {
    echo 'You searched for ' . htmlspecialchars($query) . '<br />';
    for ($i = 0; $i < $num_hosts; $i++) {
    	$id[] = yaz_connect($host[$i]);
    	$fields = array(
	  "ti" => "1=4",
	  "au"   => "1=1",
	  "isbn" => "1=7"
	);
    	yaz_ccl_conf($id[$i], $fields);  // see example for yaz_ccl_conf
	if (!yaz_ccl_parse($id[$i], $query, &$cclresult)) {
	    echo 'Error: ' . $cclresult["errorstring"];
	} else {
	    $rpn = $cclresult["rpn"];
	    //yaz_search($id, "rpn", $rpn)ů
	    yaz_syntax($id[$i], "usmarc");
	    yaz_range($id[$i], 1, 10);
	    yaz_search($id[$i], "rpn", $rpn);
	}
        
    }
    yaz_wait();
    for ($i = 0; $i < $num_hosts; $i++) {
        echo '<hr />' . $host[$i] . ':';
        $error = yaz_error($id[$i]);
        if (!empty($error)) {
            echo "Error: $error";
        } else {
            $hits = yaz_hits($id[$i]);
            echo "Result Count $hits";
        }
        echo '<dl>';
        for ($p = 1; $p <= 10; $p++) {
            $rec = yaz_record($id[$i], $p, "string");
            if (empty($rec)) continue;
            echo "<dt><b>$p</b></dt><dd>";
            echo nl2br($rec);
            echo "</dd>";
        }
        echo '</dl>';
    }
}


?>
</body>
