<?php 
define("field_not_exist","field doesn`t exist");
  define("error_message","Error occured: ");
  define("bad_data","multiple fields, should be just one");
class CiteData{
    private static $instance;
    private $data=array();
    private $fieldslist=array();
    public $current=array();
    private function __construct()
    {}

    public static function getInstance()
    {
        if (self::$instance === NULL) {
            self::$instance = new self;
        }
        return self::$instance;
    }
    protected function uniqueKey($key,$suffix){
      $instance=self::getInstance();
      if(array_key_exists($key,$instance->data))return $instance->uniqueKey($key.$suffix,++$suffix);
      return $key;
    }
    protected function getKey($data){
      if(count($data)<1)throw new Exception("Empty record");
      $key=array_key_exists("key",$data)?$data["key"]:strtolower($data["author"][0]["last"].$data["date"]);
      return self::uniqueKey($key,"a");
    }
    public function loadData($data){
      $instance=self::getInstance();
      for($i=0;$i<count($data);$i++){
        try{
          $key= $instance->getKey($data[$i]);
          $instance->data[$key]=$data[$i];
        }catch(Exception $e){}
      }
      reset($instance->data);
      $instance->setCurrent();
      //print_r($instance->data);
      return $this;
    }
    public function getCiteKey(){
      $instance=self::getInstance();
      return $instance->current["key"];
    }
    public function setCurrent(){
      $instance=self::getInstance();
      $data=current($instance->data);
      $key=key($instance->data);
      if($data){
        $instance->current=array("key"=>$key,
        "data"=>$data);
      }else{
        $instance->current=false;
      }
    }
    public function getData($field){
      $instance=self::getInstance();
      if(array_key_exists($field,$instance->current["data"])){
        /*$list=array();
        foreach($instance->fieldslist[$field] as $record){
          $list[]=$instance->data[$record];
        }
        return $list;*/
        return $instance->current["data"][$field];
      }else{
        //throw new Exception(field_not_exist);
        return null;
      }
    }
    public function records($records){
      $instance=self::getInstance();
      $instance->keys=is_array($records)?$records:$instance->key;
    }
    public function current(){
      $instance=self::getInstance();
      return is_array($instance->current)===false?false:true;
    }
    public function next(){
      $instance=self::getInstance();
      each($instance->data);
      $instance->setCurrent();
      return is_array($instance->current)===false?false:true;
    }
    public function getField($field){
      try{
        $instance=self::getInstance();
        $list=$instance->getData($field);                  
        if(count($list)==1){
          return($list);
        }else{
          throw new Exception(bad_data);
        }
      }catch(Exception $e){
        $instance->printError($e);
      }
    }
    public function getList($field){
        $instance=self::getInstance();
        return $instance->getData($field);
    }
    public function printError(Exception $e){
      echo "<div style=\"border:1px solid black;color:red;\">".error_message.$e->getMessage()."</div>\n";
    }

}


?>